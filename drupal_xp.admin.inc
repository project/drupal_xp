<?php

/**
 * @file
 * Admin page callbacks for Drupal XP.
 */

/**
 * Menu callback: displays the site status report. Can also be used as a pure check.
 *
 * @param $check
 *   If true, only returns a boolean whether there are system status errors.
 */
function drupal_xp_system_status($check = FALSE) {
  include_once DRUPAL_ROOT . '/modules/system/system.admin.inc';
  $system_status = system_status($check);
  $xp_status = str_replace(VERSION, 'XP (' . VERSION . ')', $system_status);
  return $xp_status;
}
